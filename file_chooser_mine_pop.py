import gi 

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk

#this is a class to create a file chooser window 
class FileChooserWindow():
    def __init__(self):
    
        gladefilefilechooser = "filechooser.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(gladefilefilechooser)
        self.chooser = self.builder.get_object("GtkFileChooserWindow")
        self.chooser.connect("delete-event",gtk.main)
        self.okChooser = self.builder.get_object("open_marked_file")
        self.chooser.show()


    def filechooser(self):
    
        try:
            path=None
            print("this is the file chooser method")
            response = self.chooser.run()

            if response == gtk.ResponseType.OK:
                print("open button on file chooser clicked")
                path=self.chooser.get_filename()
                print(path)
                if path == None:
                    print ("nothing selected")
            elif response == gtk.ResponseType.CANCEL:
                print("the cancel button on file chooser has pressed")

            self.chooser.destroy()
        except Exception as e: 
            print(e)
            print ("error on file chooser")
        return path            