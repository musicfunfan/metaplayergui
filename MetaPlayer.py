#python imports is from python python libs you can install them from pip or your system package manager
# my imports is in the directory of the program 


#python modules
import webbrowser
import copy
import locale
import gi
import mpv

#my modules
import metadata 
import youtube_search
import file_chooser_mine_pop

from gi.repository import Gtk as gtk
gi.require_version('Gtk', '3.0')


#this is the main class of the program 
class Main():

    def __init__(self):
       
        self.player = mpv.MPV(input_default_bindings=True, input_vo_keyboard=True, osc=True)

        #define the main window
        gladeFile = "./test.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(gladeFile)
        self.builder.connect_signals(self)

        # define the play button
        play = self.builder.get_object("button_play")
        play.connect("clicked", self.play_stuff)

        # define the main window
        self.window = self.builder.get_object("main_window")
        self.window.connect("delete-event", gtk.main_quit)
        

        # define local button 
        local = self.builder.get_object("local")
        local.connect("clicked", self.loadfile)

        #define youtube buttom
        youtube = self.builder.get_object("youtube")
        youtube.connect("clicked",self.youtube_mode)

        #define custom file button
        custom = self.builder.get_object("master_search")
        custom.connect("clicked", self.master_search_mode)

        #define the url button 
        url = self.builder.get_object("url")
        url.connect("clicked", self.show_url_pop)
        
        #define the stop button 
        stop = self.builder.get_object("stop_button")
        stop.connect("clicked",self.stop_stuff)

        #define the clear button
        clear = self.builder.get_object("clear_button ")
        clear.connect("clicked",self.cleare_screens)

        #define the save button for the main window
        self.save_button = self.builder.get_object("save_metadata_button")
        self.save_button.connect("clicked" , self.save_meta)   
    
        #define the the url path (string)
        self.url_path = None

        #define the list title
        self.title_list=[]
        
        #define the links list
        self.links = []

        #define the list of metadata to display from the song
        self.metadata_song = []

        #define the list for the history pop up window

        #define the list store for the metadata 
        self.meta_list_store = gtk.ListStore(str)

        #this is the treeview for metadata 
        self.main_meta_tree_view=self.builder.get_object("meta_tree_view")

        #define a list of metadata to display after search
        self.meta_after_search = []

        #define the curent media metadata to store them on history
        self.song_name_to_save_in_history = None
        self.artist_to_save_in_history = None
        self.genre_to_save_in_history = None
        self.year_to_save_in_history = None
        self.comment_to_add = None
        self.history = []

        #define the list store for search history
        self.search_meta_list_store = gtk.ListStore(str)

        #define the treeview for the search metadata history
        self.main_meta_tree_view_history=self.builder.get_object("meta_tree_view_history")
        
        #define the tree view items for metadata
        self.renderer_meta_main = gtk.CellRendererText()
        self.path_colum_meta_main = gtk.TreeViewColumn(title="metadata",cell_renderer=self.renderer_meta_main , text=0)

        #define the treeview items for the history metadata
        self.renderer_search_meta = gtk.CellRendererText()
        self.path_clum_meta_main_searched = gtk.TreeViewColumn(title="previus from this artist",cell_renderer=self.renderer_search_meta , text=0)

        #define the add button on main window
        self.add_button_main = self.builder.get_object("comment_button")
        self.add_button_main.connect("clicked" , self.add_comment)
        
        #define the add title button
        self.add_title_button_main = self.builder.get_object("add_title_main")
        self.add_title_button_main.connect("clicked" , self.add_title_to_the_history)

        #define the add artist button 
        self.add_artist_button = self.builder.get_object("add_artist_button")
        self.add_artist_button.connect("clicked" , self.add_artist_to_the_history)
        
        #define the add genre button 
        self.add_genre_button = self.builder.get_object("add_genre_button")
        self.add_genre_button.connect("clicked" , self.add_genre_to_the_history)

        #define the add year button
        self.add_year_button = self.builder.get_object("add_year_button")
        self.add_year_button.connect("clicked" , self.add_year_to_the_history)

        #define the selected object for the history pop up 
        self.selection_history = self.builder.get_object("history_selection")
        self.selection_history.connect("changed" , self.selectet_item_history)
        
        #define the list store for history
        self.TitlesListStoreHist = gtk.ListStore(str)
        
        self.base_history = metadata.metadata().UpdateTheHystory(song_name=self.song_name_to_save_in_history , 
                artist=self.artist_to_save_in_history , genre=self.genre_to_save_in_history ,year= self.year_to_save_in_history,
                pllist=False , comment=self.comment_to_add , add_comn=False , add_artist = False , 
                add_genre = False , add_year = False , add_title = False)    
        print("this is the constractor")
        print(self.base_history)
    
    def start_gui(self):
        self.window.show()

     #method to play url      
    def play_url(self , url_path_to_play):
        
        
        if url_path_to_play == None:
            print("no youtube url")
        else:
            print("path for media is in ")

        print(url_path_to_play)

        self.player.play(url_path_to_play)
        self.player.wait_for_event()
        #self.stop_media()
        
        
    #method to play-pause media
    def pause_play_media(self):
        pass

    #method to stop the playback 
    def stop_media(self):
        self.player.command("stop")
    
    
    def load_link(self , link):
        self.url_path = link
        print("this is the link in load_link method")
        print(link)
        print(self.url_path)
    
    
    # this is the method for the play button
    def play_stuff(self, media_stuff ):
        print("this play is prested")
        print("this is in the play method")
        print(self.url_path)
        self.play_url(url_path_to_play=self.url_path)
        
    #this is a method for the stop button (tthis method is not working)
    def stop_stuff(self , widget):
        self.stop_media()
        print ("the stop has pressed")
    
    # this is the method for the open button
    def loadfile(self, widget):
        print("this is the local button")
        chooser=file_chooser_mine_pop.FileChooserWindow()
        #chooser.filechooser()
        self.url_path=chooser.filechooser()
        try:
            print("this is the local file metadata working")
            local_file_meta=metadata.metadata().audiometadata(path=self.url_path , itemlist=None , pllist=False)
            self.take_metadata_list_and_print(title=local_file_meta[0],artist=local_file_meta[1],description="no inf0",year=local_file_meta[3] , album=local_file_meta[10])
            metadata.metadata().LoadTheHistory()
            self.base_history=metadata.metadata().UpdateTheHystory(song_name=local_file_meta[0], artist=local_file_meta[1], 
            genre=local_file_meta[2],year=local_file_meta[3],pllist=False,comment=None , add_comn = False , add_artist=False ,
            add_genre=False , add_title = False , add_year = False)
            self.song_name_to_save_in_history = local_file_meta[0]
            self.artist_to_save_in_history = local_file_meta[1]
            self.genre_to_save_in_history = local_file_meta[2]
            self.year_to_save_in_history = local_file_meta[3]
            print ("this is the local meta 1 ")
            print(local_file_meta[1])
            Main.take_history_metadata_and_print(self, local_file_meta = local_file_meta[1])
            self.take_history_metadata_and_print(local_file_meta=local_file_meta)
        except:
            print ("nothing is loaded from local")
    
    #method to run when the youtube button is clicked
    def youtube_mode(self,widget):
        print("this is the youtube button")
       
        #define the youtube window
        gladefilePopYoutube= "youtube_pop_up.glade"
        self.builderYoutube = gtk.Builder()
        self.builderYoutube.add_from_file(gladefilePopYoutube)
        self.youtubeWindow = self.builderYoutube.get_object("youtube_pop")
        self.youtubeWindow.connect("delete-event", self.delete_youtube_window)
        
        #define the search button 
        self.search_youtube_button = self.builderYoutube.get_object("search_youtube_button")
        self.search_youtube_button.connect("clicked" , self.youtube_take_entry)
        
        #define the youtube box (grid)
        self.box_grid = self.builderYoutube.get_object("box")
        
        #define the cancel button 
        self.cannelButton = self.builderYoutube.get_object("canselyt_button")
        self.cannelButton.connect("clicked", self.delete_youtube_window)
        
        #define the for youtube
        self.YoutubeTreeView = self.builderYoutube.get_object("youtube_treeView")
        
        #define the list store for youtube
        self.TitlesListStore = gtk.ListStore(str)

        #define the list strore for histroy pop up
        self.history_store = gtk.ListStore(str)

        #define the selected object for the youtube menu
        self.selection=self.builderYoutube.get_object("yt_selected")
        self.selection.connect("changed" , self.select_item)

        #define the user selected value
        self.user_selected_value=None
        self.youtubeWindow.show()
        
        
    #method to run when search for artist in the csv file 
    def master_search_mode(self,widget):
        list_to_print_pop_pup = []
        glade_file_for_searchartist = "search_artist_pop_pup.glade"
        self.builder_search_artist = gtk.Builder()
        self.builder_search_artist.add_from_file(glade_file_for_searchartist)
        self.search_window = self.builder_search_artist.get_object("search_artist")
        self.search_window.connect("delete-event", self.delete_search_artist_box)
        
        #difine the search buton 
        self.search_artist_button = self.builder_search_artist.get_object("ok_button_searchartist")
        self.search_artist_button.connect("clicked" , self.take_entry_for_search)
        
        #define the search cansel button 
        self.cansel_button_for_search_button = self.builder_search_artist.get_object("cancel_buttona_searchartist")
        self.cansel_button_for_search_button.connect("clicked" , self.delete_search_artist_box)
        
        #define the treeview for the search artist 
        self.treeview_for_searchartist = self.builder_search_artist.get_object("treeview_for_searchartist")

       
        #define the gtk title list store for search artist 
        self.search_search_artist = gtk.ListStore(str)
        
        #define the user selected value for the search value
        self.user_selected_value_search_artist = None
        
        self.search_window.show()
        
        
    #method to respone when the search button pressed
    def youtube_take_entry(self , widget):
        search_term = None
        try:    
            
            youtube_entry = self.builderYoutube.get_object("youtube_keyward")
            search_term=youtube_entry.get_text()
            print(youtube_entry.get_text())
            youtube_search_object  = youtube_search.youtubeSearch()
        
        except:
            print ("nothing to search")    
        
        try:
            titleList = youtube_search_object.search(results_number=10,keyward=search_term)
            self.take_list_and_print(Title_list=titleList[0] , links_list=titleList[1])
        except Exception as e: 
            print ("nothing is loaded on youtube search !!!")
            print (e)
        
        
        return search_term

    #def master_search_artist_get_entry(self):
    #    self.search_window.show()

        
        
    def take_entry_for_search(self , widget):
            search_term = None
            try:
                self.search_search_artist.clear()
            except:
                print("nope")
            search_artist_entry = self.builder_search_artist.get_object("entry_for_searchartist")
            search_term = search_artist_entry.get_text()
            
            artist_searched = metadata.metadata().searchhystory(filename = "clean.csv" , searchartist = search_term , plst="off" , list=None)


            self.master_search_take_and_print(artist=artist_searched[0], title=artist_searched[1], genre=artist_searched[2
            ], year=artist_searched[3])
    
         
    
    #this is method is for render the search stuff on search artist
    def master_search_take_and_print(self , artist , title , genre , year ):
        
        list_in_line =[]
        list_to_print = []
        list_to_print.append(artist)
        list_to_print.append(title)
        list_to_print.append(genre)
        list_to_print.append(year)
        
        a=len(list_to_print)
        print (a)
        artist_line =[]
        title_line = []
        genere_line = []
        year_line = []
        for i in range(a):
            artist_line.append(list_to_print[0])
            title_line.append(list_to_print[1])
            genere_line.append(list_to_print[2])
            year_line.append(list_to_print[3])
            artists = copy.deepcopy(artist_line[0])
            titles = copy.deepcopy(title_line[0])
            generes = copy.deepcopy(genere_line[0])
            years = copy.deepcopy(year_line[0])
            f=0
        while f < len(artists):
            list_in_line.append("artist:")
            list_in_line.append(artists[f])
            list_in_line.append("title:")
            list_in_line.append(titles[f])
            self.meta_after_search.append(titles[f])
            list_in_line.append("genre:")
            list_in_line.append(generes[f])
            list_in_line.append("year:")
            list_in_line.append(years[f])
            f=f+1
        
        
        for stuff in list_in_line:
            self.search_search_artist.append([stuff])   
            
        renderer = gtk.CellRendererText()
        pathColumn = gtk.TreeViewColumn(title="results" , cell_renderer=self.renderer_meta_main , text=0)
        self.treeview_for_searchartist.append_column(pathColumn)
        self.treeview_for_searchartist.set_model(self.search_search_artist)
            
        

    #this is a method to render the results in the gtk box for youtube window
    def take_list_and_print(self , Title_list , links_list):
        
        for links in Title_list:
            self.TitlesListStore.append([links])
            
        renderer = gtk.CellRendererText()
        pathColumn = gtk.TreeViewColumn(title="results",cell_renderer=renderer , text=0)
        self.YoutubeTreeView.append_column(pathColumn)
        self.YoutubeTreeView.set_model(self.TitlesListStore)
        self.title_list = Title_list
        self.links = links_list
        
    #this is a method to render the metadata in main window
    def take_metadata_list_and_print(self , title , artist ,album ,description , year):
        self.metadata_song.clear()
        self.metadata_song.append("title:")
        self.metadata_song.append(title)
        self.metadata_song.append("artist:")
        self.metadata_song.append(artist)
        self.metadata_song.append("album:")
        self.metadata_song.append(album)
        self.metadata_song.append("year:")
        self.metadata_song.append(year)
        self.metadata_song.append("description:")
        self.metadata_song.append(description)
        
        print("this is metalist store")
        print(self.meta_list_store)
        for data in self.metadata_song:
            self.meta_list_store.append([data])
        
        
        self.main_meta_tree_view.append_column(self.path_colum_meta_main)
        self.main_meta_tree_view.set_model(self.meta_list_store)
        
    #this is a method to render the history searched metadata if available on mainwindow
    def take_history_metadata_and_print(self , local_file_meta):
        display_extra = []
        self.meta_after_search.clear()
        print("this is the local file")
        print(local_file_meta)
        #try:
        self.history = metadata.metadata().searchhystory(filename="clean.csv",searchartist=local_file_meta,plst="off",list=None)
        print("this is the local_file_meta[1]")
        print(local_file_meta)
        
        a=len(self.history)
        print (a)
        history_artist =[]
        history_title = []
        history_genere = []
        history_year = []
        for i in range(a):
            history_artist.append(self.history[0])
            history_title.append(self.history[1])
            history_genere.append(self.history[2])
            history_year.append(self.history[3])
            artists = copy.deepcopy(history_artist[0])
            titles = copy.deepcopy(history_title[0])
            generes = copy.deepcopy(history_genere[0])
            years = copy.deepcopy(history_year[0])
            f=0
        while f < len(artists):
            display_extra.append("artist:")
            display_extra.append(artists[f])
            display_extra.append("title:")
            display_extra.append(titles[f])
            self.meta_after_search.append(titles[f])
            display_extra.append("genre:")
            display_extra.append(generes[f])
            display_extra.append("year:")
            display_extra.append(years[f])
            f=f+1
        print ("this is in take history metadata and print")
        print(self.meta_after_search)
        #self.print_the_extra_metadata_history(things_to_display=display_extra)
        for meta_searched in self.meta_after_search:
            self.search_meta_list_store.append([meta_searched])
        
        self.main_meta_tree_view_history.append_column(self.path_clum_meta_main_searched)
        self.main_meta_tree_view_history.set_model(self.search_meta_list_store)
        metadata.metadata().clearThehystory()
        #exept Exception as e:
        #print(e)
        #print ("error printing the metadata on main window")

   #select the title from youtube
    def select_item(self , user_data):
        
        (lists_store , Tree_iter) = user_data.get_selected_rows()
        for item in Tree_iter:
            
            TreeIter = lists_store.get_iter(item)
            self.user_selected_value = lists_store.get_value(TreeIter,0)
            print(self.user_selected_value)
            self.connect_titles_to_ytlinks()
            self.load_link(self.link_selected)
            meta = metadata.metadata().ShowMetada(yturls=self.url_path , mode="plfalse", check="bugi")

            self.take_metadata_list_and_print(title=meta[0],artist=meta[1] ,album=meta[3] , description=meta[4] , year=meta[5])
            metadata.metadata().LoadTheHistory()
            self.base_history = metadata.metadata().UpdateTheHystory(song_name=meta[0],artist=meta[1],genre=meta[5],
            year=meta[5] , pllist = False ,comment = None , add_comn = False , add_title = False ,
            add_artist = False , add_genre = False , add_year = False)
            self.song_name_to_save_in_history = meta[0]
            self.artist_to_save_in_history = meta[1]
            self.genre_to_save_in_history = None
            self.year_to_save_in_history = meta[5]
            print("this is the meta 1 ")
            print(meta[1])
            
            self.take_history_metadata_and_print(local_file_meta=meta[1])

    #this is a method to select the song name from the histoty
    def selectet_item_history(self , user_data1):
        print("this is the selectet_item_history method running")
         
        
        things_to_print = None
        (lists_store , Tree_iter) = user_data1.get_selected_rows()
        for item in Tree_iter:
            TreeIter = lists_store.get_iter(item)
            self.user_selected_value_history = lists_store.get_value(TreeIter,0)
            print("this is the user balue history")
            print(self.user_selected_value_history)
            things_to_print = self.connect_history_metadata(history_selection=self.user_selected_value_history)
            print("this is the things to print")
            print (things_to_print)
        #This is the change 2
        if things_to_print != None and self.TitlesListStoreHist !=[]:
            self.TitlesListStoreHist.clear()
            #define the show history pop window
            self.history_window_show = gtk.Builder()
            glade_file_show_history = "show_history.glade"
            self.history_window_show.add_from_file(glade_file_show_history)
            self.history_window = self.history_window_show.get_object("history_pop_up")
            
            ##This is new stuff if does not work delete here 
            #self.history_window = self.history_window_show.get_object("ok_button_histroy_pop")
            #self.history_window.connect("clicked" ,self.close_history_show)


            self.history_window.connect("delete-event", self.delete_history_pop_show)
            self.history_window.show()
            #define the tree veiw for the pop history menu
            self.history_tree = self.history_window_show.get_object("tree_view_metadata_pop") 
            
            print("this is the things to print")
            print(things_to_print[1])
            print(self.TitlesListStoreHist)
            for links in things_to_print:
                print("this is the links ")
                print(links)
                self.TitlesListStoreHist.append([links])
            renderer = gtk.CellRendererText()
            pathColumn = gtk.TreeViewColumn(title="metadata", cell_renderer=renderer , weight_set=True , weight = 1 , text=0)
            self.history_tree.append_column(pathColumn)
            self.history_tree.set_model(self.TitlesListStoreHist)
        else:
            print("nothing to show yet on history pop up")
            
            
    def close_history_show(self ,widget):
        self.history_window.destroy()
        

        

    #this is a method to connect the song name in history with the metadata
    def connect_history_metadata(self ,history_selection):
        
        print("this is the connect history metadata method running")
        metadata_histroy = metadata.metadata().search_song(filename="clean.csv" , search_song=history_selection)
        print("this is the metada history")
        print(metadata_histroy)
        meta_list_user = []
        meta_list_user.append(metadata_histroy[0])
        meta_list_user.append(metadata_histroy[1])
        meta_list_user.append(metadata_histroy[2])
        meta_list_user.append(metadata_histroy[3])    
        meta_list_user.append(metadata_histroy[4])
        print("this is the metadata_history")
        print(metadata_histroy)
        return meta_list_user
    
    #this method will connect all the results in the gtk window with youtube link
    def connect_titles_to_ytlinks(self):
        print("this is the method to connect the tittles")
        tititle_backup = []
        tititle_backup = copy.deepcopy(self.title_list)
                
        i=0
        #self.links.remove(None)
        for items in self.title_list:
            
            self.title_list[i] = self.links[i]
            
            if self.user_selected_value == tititle_backup[i]:
                user_requested_url = self.links[i]
                print("this is the user requested url")
                print (user_requested_url)
                self.link_selected = user_requested_url
            i=i+1
        self.youtubeWindow.destroy()
        return tititle_backup , self.link_selected

    #this is a method to destroy the youtube pop up window
    def delete_youtube_window(self , widget , *args):
        self.youtubeWindow.destroy()
         
    #this is a method to clean the data displayd in main window 
    def cleare_screens(self, wiget):
        try:
            print("clear the metadata display selected")
            
            self.TitlesListStoreHist.clear()
            self.meta_list_store.clear()
            self.search_meta_list_store.clear()
            
            
        except:
            print("nothing loaded")
    #this is a method to take a comment from the user  
    def add_comment(self , widget):
        #define the add button window
        glade_file_add_comment_box = "add_pop_up.glade"
        self.builder.add_from_file(glade_file_add_comment_box)
        self.add_comment_box = self.builder.get_object("open_pop_up")
        self.add_comment_box.connect("delete-event", self.delete_add_comment_box)

        #define the cansel button for the add comment window 
        self.cansel_add_box = self.builder.get_object("cansel_button_add_box")
       
        #define the add commnet button 
        self.add_button = self.builder.get_object("add_button")
        
        comment_to_add = None
        print("this is the add comment method")
        self.add_comment_box.show()
        #try:
        responce =  self.add_comment_box.run()
        if responce == -5:
            add_entry = self.builder.get_object("add_entry")
            self.comment_to_add = add_entry.get_text()
            self.base_history = metadata.metadata().UpdateTheHystory(song_name=self.song_name_to_save_in_history , 
            artist=self.artist_to_save_in_history , genre=self.genre_to_save_in_history ,year= self.year_to_save_in_history,
            pllist=False , comment=self.comment_to_add , add_comn=True , add_artist = False , 
            add_genre = False , add_year= False , add_title = False)
            self.add_comment_box.destroy()
        if responce == -6:
            self.add_comment_box.destroy()
        #except Exception as e:
        #    print("error on the add comment")
        #    print(e)

    
    #this is a method to add the title 
    def add_title_to_the_history(self , widget):
        print("this is the add title method")
        
        #define the add title window 
        self.add_title_builder = gtk.Builder()
        glade_file_add_title = "add_title_pop_up.glade"
        self.add_title_builder.add_from_file(glade_file_add_title)
        self.add_title_box = self.add_title_builder.get_object("open_pop_up_title")
        self.add_title_box.connect("delete-event", self.delete_add_title_box)
        
        responce = self.add_title_box.run()
        if responce == -5:
            add_title_entry = self.add_title_builder.get_object("add_entry_title")
            self.song_name_to_save_in_history = add_title_entry.get_text()
            print("this is the add title")
            print(self.song_name_to_save_in_history)
            print(add_title_entry.get_text())
            self.base_history = metadata.metadata().UpdateTheHystory(song_name=self.song_name_to_save_in_history , 
            artist=self.artist_to_save_in_history , genre=self.genre_to_save_in_history ,year= self.year_to_save_in_history,
            pllist=False , comment=self.comment_to_add , add_comn=False , add_artist = False , 
            add_genre = False , add_year= False , add_title = True)
            self.add_title_box.destroy()
        if responce == -6:
            self.add_title_box.destroy()

    #this is a method to add artist 
    def add_artist_to_the_history(self , widget):
        print("this is the add artist method")
        #define the add artist window
        self.add_artist_builder = gtk.Builder()
        glade_file_add_artist = "pop_up_add_artist.glade"
        self.add_artist_builder.add_from_file(glade_file_add_artist)
        self.add_artist_box = self.add_artist_builder.get_object("open_pop_up_artist")
        self.add_artist_box.connect("delete-event" , self.delete_add_artist_box)
        
        
        responce = self.add_artist_box.run()
        if responce == -5:
            add_artist_entry = self.add_artist_builder.get_object("add_entry_artist")
            self.artist_to_save_in_history = add_artist_entry.get_text()
            self.base_history = metadata.metadata().UpdateTheHystory(song_name=self.song_name_to_save_in_history , 
            artist=self.artist_to_save_in_history , genre=self.genre_to_save_in_history ,year= self.year_to_save_in_history,
            pllist=False , comment=self.comment_to_add , add_comn=False , add_artist = True , 
            add_genre = False , add_year= False , add_title = False)
            self.add_artist_box.hide()
        if responce == -6:
            self.add_artist_box.destroy()

    def add_genre_to_the_history(self , widget):
        print("this is the add genre method")
        
        #define the add genre window 
        self.add_genre_builder = gtk.Builder()
        glade_file_add_genre = "add_genre_pop_up.glade"
        self.add_genre_builder.add_from_file(glade_file_add_genre)
        self.add_genre_box = self.add_genre_builder.get_object("open_pop_up_genre")
        self.add_genre_box.connect("delete-event" , self.delete_add_genere_box)
        
        respone = self.add_genre_box.run()
        if respone == -5:
            add_genre_entry = self.add_genre_builder.get_object("add_entry_genre")
            self.genre_to_save_in_history = add_genre_entry.get_text()
            self.base_history = metadata.metadata().UpdateTheHystory(song_name=self.song_name_to_save_in_history , 
            artist=self.artist_to_save_in_history , genre=self.genre_to_save_in_history ,year= self.year_to_save_in_history,
            pllist=False , comment=self.comment_to_add , add_comn=False , add_artist = False , 
            add_genre = True , add_year= False , add_title = False)
            self.add_genre_box.destroy()
        if respone == -6:
            self.add_genre_box.destroy()

    #this is a method to add the year in the history 
    def add_year_to_the_history(self , widget):
        
        print("this is the add year method")
        
        #define the add year window
        self.add_year_builder = gtk.Builder()
        glade_file_add_year = "pop_up_add_year.glade"
        self.add_year_builder.add_from_file(glade_file_add_year)
        self.add_year_box = self.add_year_builder.get_object("open_pop_up_year")
        self.add_year_box.connect("delete-event" , self.delete_add_year_box)
        
        responce = self.add_year_box.run()
        if responce == -5:
            add_year_entry = self.add_year_builder.get_object("add_entry_year")
            self.year_to_save_in_history = add_year_entry.get_text()
            self.base_history = metadata.metadata().UpdateTheHystory(song_name=self.song_name_to_save_in_history , 
            artist=self.artist_to_save_in_history , genre=self.genre_to_save_in_history ,year= self.year_to_save_in_history,
            pllist=False , comment=self.comment_to_add , add_comn=False , add_artist = False , 
            add_genre = False , add_year = True , add_title = False)
            self.add_year_box.destroy()
        if responce -6:
             self.add_year_box.destroy()

    #this is a method to show url pop up window
    def show_url_pop(self , widget):
        
        gladeFileUrlPopup = "dialog_url.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(gladeFileUrlPopup)
        self.urlWindow = self.builder.get_object("dialog_pop_url")
        self.urlWindow.connect("delete-event",self.delete_pop_window_url)
        self.ok_urlButton = self.builder.get_object("ok_button_url")
        self.ok_urlButton.connect("clicked" , self.url_button_ok)
        self.url_cancel = self.builder.get_object("cansel_button_url")
        self.url_cancel.connect("clicked", self.delete_pop_window_url)
        self.url_show_sites = self.builder.get_object("see_the_sites_button")
        self.url_show_sites.connect("clicked" , self.show_sites_in_browser)
        self.urlWindow.show()

    
    #this is a method to pass the url to the player method 
    def url_button_ok(self , widget):
        print("the ok button working")
        #create and take the content from the text box 
        entry_url = self.builder.get_object("entry_url")
        path=entry_url.get_text()
        self.urlWindow.destroy()
        print ("this is in pop url methoid")
        print(path)
        self.url_path = path
        url_meta=metadata.metadata().ShowMetada(yturls=self.url_path , mode="plfalse" , check=None)
        print("this is the url_meta 0")
        print(url_meta[0])
        
        self.take_metadata_list_and_print(title=url_meta[0] , artist=url_meta[1]
        , album=url_meta[3] , description=url_meta[4] , year=url_meta[5] )
        self.take_history_metadata_and_print(local_file_meta=url_meta[0])
        
        metadata.metadata().LoadTheHistory()
        

        self.base_history = metadata.metadata().UpdateTheHystory(song_name = url_meta[0] ,artist=url_meta[1],genre="no info",
        year=url_meta[5] , pllist = False ,comment = None , add_comn = False , add_title = False ,
        add_artist = False , add_genre = False , add_year = False)
        self.song_name_to_save_in_history = url_meta[0]
        self.artist_to_save_in_history = url_meta[1]
        self.genre_to_save_in_history = None
        self.year_to_save_in_history = url_meta[5]

        #This is new this is to delete if program not working normal 
        Main.take_history_metadata_and_print(self, local_file_meta = url_meta[1])
        print ("This is the url_meta[1]")
        print (url_meta[1])
        print("this is the base history")
        print(self.base_history)

    #this is a method to destroy the url pop window
    def delete_pop_window_url(self , widget,*args):
        self.urlWindow.destroy()
    

     #this is a mehtod to open a browser and inform the user for the supported sites
    def show_sites_in_browser(self , widget):
        print("this is the browser method")
        url = "https://github.com/yt-dlp/yt-dlp/blob/master/supportedsites.md"
        webbrowser.open_new(url)


    #this is a mthod to to cloase the add window
    def delete_add_comment_box(self, widget , *args ):
        self.add_comment_box.destroy()

    #this is for destroing the add title window
    def delete_add_title_box(self , widget , *args):
        self.add_title_box.destroy()

    #this is for destroing the add artist window
    def delete_add_artist_box(self , wideget , *args):
        self.add_artist_box.destroy()
   
    #this is a method for destroing add genre window
    def delete_add_genere_box(self , widget , *args):
        self.add_genre_box.destroy()

    #this is a method for destroing add year window
    def delete_add_year_box(self , widget , *args):
        self.add_year_box.destroy()
    
    #this is a method for destroing the history pop up window
    def delete_history_pop_show(self , widget , *args):
        self.history_window.destroy()
    #this is a method to destroing the search artist pop window
    def delete_search_artist_box(self  , widget , *args):
        self.search_window.destroy()
    
    #this is a method to save metadata
    def save_meta(self , widget):
        print("this is the base history")
        print(self.base_history)
        metadata.metadata().save_the_metadata(base=self.base_history)
        metadata.metadata().clearThehystory()
        

    
if __name__ == '__main__':
    locale.setlocale(locale.LC_NUMERIC, 'C')
    main = Main().start_gui()
    gtk.main()
    
        


    
    
    
    
    
    
   

       
        

   