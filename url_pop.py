import gi 

gi.require_version('Gtk' , '3.0')
from gi.repository import Gtk as gtk

class PopUrlDialog():
    def __init__(self):

        gladeFileUrlPopup = "dialog_url.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(gladeFileUrlPopup)
        self.urlWindow = self.builder.get_object("dialog_pop_url")
        self.urlWindow.connect("delete-event",self.delete_pop_window_url)
        self.ok_urlButton = self.builder.get_object("ok_button_url")
        self.urlWindow.show()

    def url(self):
        path = None
        print("this is the url button ")
        responce = self.urlWindow.run()
        if responce == -5:
            print("the ok button working")
            #create and take the content from the text box 
            entry_url = self.builder.get_object("entry_url")
            path=entry_url.get_text()
            self.urlWindow.destroy()
            print ("this is in pop url methoid")
            print(path)
        if responce == -6:
            print("the cansel button working")

        self.urlWindow.destroy()
        return path

    def delete_pop_window_url(self , widget,*args):
        self.urlWindow.destroy()