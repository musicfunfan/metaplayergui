from contextlib import ExitStack
from tinytag import TinyTag
from yt_dlp import YoutubeDL
ydl = YoutubeDL()
ydl.add_default_info_extractors
from more_itertools import unique_everseen
import csv 
import shutil


#this class take the metadata
class metadata():


    def audiometadata(self ,path ,itemlist ,pllist):
        comms=["None"]
        comm=None
        if pllist==False:
            audio = TinyTag.get(path)
            Title=str((audio.title))
            print("Title:" + Title)
            Artist=str(audio.artist)
            print("Artist: " + Artist )
            Genre=str((audio.genre))
            print("Genre:" + Genre)
            Year=str(audio.year)
            print("Year Released: " +  Year)
            Bitrate=audio.bitrate
            print("Bitrate:" + str(Bitrate) + " kBits/s")
            Composer=audio.composer
            print("Composer: " + str(Composer))
            Filesize=audio.filesize
            print("Filesize: " + str(Filesize) + " bytes")
            Albumartist=audio.albumartist
            print("AlbumArtist: " + str(Albumartist))
            Duration=audio.duration
            print("Duration: " + str(Duration) + " seconds")
            TrackTotal=audio.track_total
            print("TrackTotal: " +str(TrackTotal))
            #comm=input("any comment")
        Titles=["None"]
        Artists=["None"]
        Genres=["None"]
        Years=["None"] 
        
        #this is a list for the playlist option 
     
        if pllist==True:
            
            
            Title="None"
            Artist="None"
            Genre="None" 
            Year="None"
            a=len(itemlist)
            i=0
         
            Titles.remove("None")
            Artists.remove("None")
            Genres.remove("None")
            Years.remove("None")
            comms.remove("None")
            sep="'"
            sep1="'"
            
            for k in range(a):
                
              
                audio = TinyTag.get(itemlist[i])
                Title=str((audio.title))
                print("Title:" + Title)
                Title1=sep+Title+sep1
                Artist=str(audio.artist)
                print("Artist: " + Artist )
                Genre=str((audio.genre))
                print("Genre:" + Genre)
                Year=str(audio.year)
                print("Year Released: " +  Year)
                Bitrate=audio.bitrate
                print("Bitrate:" + str(Bitrate) + " kBits/s")
                Composer=audio.composer
                print("Composer: " + str(Composer))
                Filesize=audio.filesize
                print("Filesize: " + str(Filesize) + " bytes")
                Albumartist=audio.albumartist
                print("AlbumArtist: " + str(Albumartist))
                Duration=audio.duration
                print("Duration: " + str(Duration) + " seconds")
                TrackTotal=audio.track_total
                print("TrackTotal: " + str(TrackTotal))
                #loading the list for the metadata
               
                
                Titles.append(Title)
                Artists.append(Artist)
                Genres.append(Genre)
                Years.append(Year)

                #comm=input("any comment")
                #comms.append(comm)
                i=i+1
                 
        return Title,Artist,Genre,Year,Titles,Artists,Genres,Years,comm,comms,Albumartist      
        
    #this method takes the metadata from youtube
    def ShowMetada(self, yturls ,mode ,check):
       
        artists=["None"]
        titles=["None"]
        song_names=["None"]
        quetions=["None"]
        artist="None"
        title="None"
        song_name="None"
        quetion="None"
        if mode=="pltru":
            yturls.remove('')
     
            i = len(yturls)
            a=0
            #for youtube metada
            for x in range(i):
                
                try:
                    with ydl:
                        result = ydl.extract_info(yturls[a], download=False,)
                        try:
                        
                            song_name = result["track"]
    
                            print("name of the song  \n"+song_name)
                            song_names.append(song_name)
                        except:
                            print("error load the name")
                        try:
                            artist = result["artist"]
                            print(" artist \n"+artist)
                            artists.append(artist)
                        except:
                            print ("error loading the artist")
                          
                        try:
                            title = result['title']
                            print(i, title)
                            titles.append(title)
                        except:
                            print("error loading the title")
                            album=result['album']
                        try:
                            print("album" + album)
                            print(i, title)
                        except:
                            print("error loading the album")
                            
                            description = result['description']
                            print("this is the description  \n" + description)
                        quetion=input("do you have a comment on this track")
                        quetions.append(quetion)
                        #artists.remove("None")
                        #song_names.remove("None")
                        #titles.remove("None")
                        #quetions.remove("None")
                        
                            
                        a=a+1
                except Exception as e:
                    print(e)
                    print("error on loading files")
          
        if mode=="plfalse":
            
            year = None
            description = None
            album = None
            print("this is the  yturls")
            print(yturls)
            
            try:
                with ydl:
                    result = ydl.extract_info(yturls, download=False,)
                    try:
                        song_name = result["track"]
                        print("name of the song  \n"+song_name)
                        song_names.append(song_name)
                    except:
                        print("error load the name")
                    try:
                        artist = result["artist"]
                        print(" artist \n"+artist)
                        artists.append(artist)
                    except:
                        print ("error loading the artist")
                        artist = "no info"
                    try:
                        title = result['title']
                        print(i, title)
                        titles.append(title)
                    except:
                        print("error loading the title")
                    try:
                        album=result['album']
                        print("album" + album)
                        print(i, title)
                    except:
                        print("error loading the album")
                        album = "no info"
                        description = result['description']
                        print("this is the description  \n" + description)
                    #quetion=input("do you have any comment ?")
                    self.title_meta = title
                    self.artist_meta = artist
                    #youtube is not provide year metadata
                    year = "no info"
            except Exception as e:
                print(e)
                print("error in show metadata method")
                
        else:
            print ("bugi")    
                
        return title,artist,song_name,album,description,year

    #this is the method to load the history
    def LoadTheHistory(self):
        
        try:
            print("this is the load the history method")
            #load csv and convert to the dictionary
            filename="output.csv"
            artist=[None]
            title=[None]
            genre=[None]
            year=[None]
            artists=[None]
            titles=[None]
            genres=[None]
            years=[None]
            i=0
            with open(filename, mode='r') as inp:
                for line in csv.reader(inp):

                    title.append(line[0])

                    artist.append(line[1])
                    genre.append(line[2])
                    year.append(line[3])
                    i=i+1
                title.remove(None)
                artist.remove(None)
                genre.remove(None)
                year.remove(None)
                artistkey=artist[0]
                titlekey=title[0]
                genrekey=genre[0]
                yearkey=year[0]
                a=len(title)

                x=0
                for i in range(a-1):
                    x=x+1
                    artists.append(artist[x])
                    titles.append(title[x])
                    genres.append(genre[x])
                    years.append(year[x])
                titles.remove(None)
                artists.remove(None)
                genres.remove(None)
                years.remove(None)    
                hystory={artistkey:[],titlekey: [],genrekey:[],yearkey:[]}
                b=len(titles)
                for c in range(b):
                    hystory[artistkey].append(artists[c])
                    hystory[titlekey].append(titles[c])
                    hystory[genrekey].append(genres[c])
                    hystory[yearkey].append(years[c])
                artistlist=hystory[artistkey]
                titlelist=hystory[titlekey]
                genrelist=hystory[genrekey]
                yearlist=hystory[yearkey]
        except Exception as e:
            print(e)
            print("error while loading the history")
                    
        return artistlist,titlelist,genrelist,yearlist,hystory

    #this is the method to update the hystory file
    def UpdateTheHystory(self,song_name,artist,genre,year,pllist,comment,add_comn,add_title,add_artist,add_year,add_genre ):
        
        print("this is the update history")
        print("this is the song name")
        print(song_name)
        
        try:
            song_name.remove("None")
        except:
            print("ok")
        try:
            artist.remove("None")
        except:
            print("ok")
        try:
            genre.remove("None")
        except:
            print("ok")
        try:
            comment.remove("None")
        except:
            print("ok")
        
        if pllist==True and add_comn == False:
            base={'Title':None,'Artist':None,'genre':None,'year':None ,'comments':None}
            h=0
            z=len(song_name)
           
            
            for x in range(z):
                try:
                    update={'Title':song_name[x],'Artist':artist[x],'genre':genre[x],'year': year[x] ,'comments':comment[x]}
                    base.update(update)
                    #print(base)
                    #write in the file
                    w = csv.writer(open("output.csv", "a"))
                    #w.writerow(base.keys())
                    w.writerow(base.values())
                    h=h+1
                    
                except:
                    print("balance")

        #this runs with the save history
        if pllist==False and add_comn == False:
            
            base={'Title':None,'Artist':None,'genre':None,'year':None, 'comments':None}
            update={'Title':song_name,'Artist':artist,'genre':genre,'year': year,'comments':comment}
            base.update(update)
          
        
        if add_comn == True and pllist == False:    
            try:
                comment.remove("None")
            except:
                print("ok")
            base={'Title':None,'Artist':None,'genre':None,'year':None, 'comments':None}
            update={'Title':song_name,'Artist':artist,'genre':genre,'year':year,'comments':comment}
            base.update(update)
            
        if add_title == True and pllist == False:
            base={'Title':None,'Artist':None,'genre':None,'year':None, 'comments':None}
            update={'Title':song_name,'Artist':artist,'genre':genre,'year':year,'comments':comment}
            base.update(update)
            print("this is the base after update")
            print(update)
        
        if add_artist == True and pllist == False:
            base={'Title':None,'Artist':None,'genre':None,'year':None, 'comments':None}
            update={'Title':song_name,'Artist':artist,'genre':genre,'year':year,'comments':comment}
            base.update(update)
        
        if add_year == True and pllist == False:
            base={'Title':None,'Artist':None,'genre':None,'year':None, 'comments':None}
            update={'Title':song_name,'Artist':artist,'genre':genre,'year':year,'comments':comment}
            base.update(update)
            print("this is the base after update")
            print(update)
        
        if add_genre == True and pllist == False:
            base={'Title':None,'Artist':None,'genre':None,'year':None, 'comments':None}
            update={'Title':song_name,'Artist':artist,'genre':genre,'year':year,'comments':comment}
            base.update(update) 
            print ("this is the base after update")
            print(update)
        return base
        
        
        
    #this is a method to save the metadata and the comments 
    def save_the_metadata(self , base):
        try:
            #write in the file
            w = csv.writer(open("output.csv", "a"))
            #w.writerow(base.keys())
            w.writerow(base.values())
            
        except Exception as e:
            print("error with the save_the_metadata method")
            print(e)
    
    
    def searchhystory(self,filename,searchartist,plst,list):
        filename="clean.csv"
        target_artist = []
        target_genre =[]
        target_year =[]   
        target_title = []     
        
        
        artist=[None]
        title=[None]
        genre=[None]
        year=[None]
        comment=[None]
        artists=[None]
        titles=[None]
        genres=[None]
        years=[None]
        comments=[None]
        
        
        if plst=="off":
            

            with open(filename, mode='r') as inp:
                for line in csv.reader(inp):
                    try:
                        title.append(line[0])
                    except:
                        print("nope")
                    try:
                        artist.append(line[1])
                    except:
                        print("nope")
                    try:
                        genre.append(line[2])
                    except:
                        print("nope")
                    try:
                        year.append(line[3])
                    except:
                        print("nope")
                    try:
                        comment.append(line[4])
                    except:
                        print("nope")
                title.remove(None)
                artist.remove(None)
                genre.remove(None)
                year.remove(None)
                comment.remove(None)
                artistkey=artist[0]
                titlekey=title[0]
                genrekey=genre[0]
                yearkey=year[0]
                commentkey=comment[0]
                a=len(title)

                x=0
                for i in range(a-1):
                    x=x+1
                    artists.append(artist[x])
                    titles.append(title[x])
                    genres.append(genre[x])
                    years.append(year[x])
                    try:
                        comments.append(comment[x])
                    except:
                        print("this is normal")
                titles.remove(None)
                artists.remove(None)
                genres.remove(None)
                years.remove(None)
                comments.remove(None)    
                hystory={artistkey:[],titlekey: [],genrekey:[],yearkey:[],commentkey:[]}
                b=len(titles)
                for c in range(b):
                    hystory[artistkey].append(artists[c])
                    hystory[titlekey].append(titles[c])
                    hystory[genrekey].append(genres[c])
                    hystory[yearkey].append(years[c])
                    hystory[commentkey].append(comment[c])
                artistlist=hystory[artistkey]
                titlelist=hystory[titlekey]
                genrelist=hystory[genrekey]
                yearlist=hystory[yearkey]
                commentlist=hystory[commentkey]
                #searchartist=artist
                index=[]
                for target, elem in enumerate(artistlist):
                    if elem==searchartist:

                        print(f"{searchartist} is found at index {target}")
                        index.append(target)
                i=0
                print("this is the hystory for the artist you listened "+ str(searchartist))
                for a in index:
                    print(searchartist,i)
                    print("-----------")
                    print(list,i)
                    print("-----------")
                    print(artistlist[index[i]])
                    print(titlelist[index[i]])
                    print(genrelist[index[i]])
                    print(yearlist[index[i]])
                    print(commentlist[index[i]])
                    try:
                        target_artist.append(artistlist[index[i]])
                    except:
                        print("nope")
                    try:
                        target_title.append(titlelist[index[i]])
                    except:
                        print("this is normal")
                    try:    
                        target_genre.append(genrelist[index[i]])
                    except:
                        print("this is normal")
                    try:
                        target_year.append(yearlist[index[i]])
                    except:
                        print("mope")
                    i=i+1
                    #print("this is inside the metadata.py")
                    #print(target_artist)
            inp.close()
        if plst=="on":
            print("this is the plst on")
            with open(filename, mode='r') as inp:
                for line in csv.reader(inp):
                    
                    title.append(line[0])
                    artist.append(line[1])
                    genre.append(line[2])
                    year.append(line[3])
                    comment.append(line[4])
                title.remove(None)
                artist.remove(None)
                genre.remove(None)
                year.remove(None)
                comment.remove(None)
                artistkey=artist[0]
                titlekey=title[0]
                genrekey=genre[0]
                yearkey=year[0]
                commentkey=comment[0]
                a=len(title)

                x=0
                for i in range(a-1):
                    x=x+1
                    artists.append(artist[x])
                    titles.append(title[x])
                    genres.append(genre[x])
                    years.append(year[x])
                    comments.append(comment[x])
                titles.remove(None)
                artists.remove(None)
                genres.remove(None)
                years.remove(None)
                comments.remove(None)    
                hystory={artistkey:[],titlekey: [],genrekey:[],yearkey:[],commentkey:[]}
                b=len(titles)
                for c in range(b):
                    hystory[artistkey].append(artists[c])
                    hystory[titlekey].append(titles[c])
                    hystory[genrekey].append(genres[c])
                    hystory[yearkey].append(years[c])
                    hystory[commentkey].append(comment[c])
                artistlist=hystory[artistkey]
                titlelist=hystory[titlekey]
                genrelist=hystory[genrekey]
                yearlist=hystory[yearkey]
                commentlist=hystory[commentkey]
                #searchartist=artist
                ba = len(list)
                for w in range(ba):
                    index=[]
                    for target, elem in enumerate(artistlist):
                        if elem==list[w]:

                            print(f"{list[w]} is found at index {target}")
                            index.append(target)
                    i=0
                    print("this is the history for the artist you listened "+ str(list[w]))
                    for a in index:
                        print(list,i)
                        print("-----------")
                        print(artistlist[index[i]])
                        print(titlelist[index[i]])
                        print(genrelist[index[i]])
                        print(yearlist[index[i]])
                        print(commentlist[index[i]])
                        i=i+1
        return target_artist , target_title, target_genre , target_year, artist,title,genre,year,artists,titles,genres,years

    def search_song(self, search_song , filename):
        print("this is the search_song")
        filename="clean.csv"
        target_artist = []
        target_genre =[]
        target_year =[]   
        target_title = []     
        
        
        artist=[None]
        title=[None]
        genre=[None]
        year=[None]
        comment=[None]
        artists=[None]
        titles=[None]
        genres=[None]
        years=[None]
        comments=[None]
         
        with open(filename, mode='r') as inp:
            for line in csv.reader(inp):
                title.append(line[0])
                artist.append(line[1])
                genre.append(line[2])
                year.append(line[3])
                try:
                    comment.append(line[4])
                except:
                    print("this is normal")
                print("this is the line 4 ")
                print("nope")
            title.remove(None)
            artist.remove(None)
            genre.remove(None)
            year.remove(None)
            comment.remove(None)
            artistkey=artist[0]
            titlekey=title[0]
            genrekey=genre[0]
            yearkey=year[0]
            commentkey=comment[0]
            a=len(title)
            x=0
            for i in range(a-1):
                x=x+1
                artists.append(artist[x])
                titles.append(title[x])
                genres.append(genre[x])
                years.append(year[x])
                try:
                    comments.append(comment[x])
                except:
                    print("this is normal")
            titles.remove(None)
            artists.remove(None)
            genres.remove(None)
            years.remove(None)
            comments.remove(None) 
            hystory={artistkey:[],titlekey: [],genrekey:[],yearkey:[],commentkey:[]}
            b=len(titles)       
        for c in range(b):
            hystory[artistkey].append(artists[c])
            hystory[titlekey].append(titles[c])
            hystory[genrekey].append(genres[c])
            hystory[yearkey].append(years[c])
            hystory[commentkey].append(comment[c])
        artistlist=hystory[artistkey]
        titlelist=hystory[titlekey]
        genrelist=hystory[genrekey]
        yearlist=hystory[yearkey]
        commentlist=hystory[commentkey]        
        print("this is the index")        
        index=[]
        for target, elem in enumerate(titlelist):
            if elem==search_song:
                print("this is the printf")
                print(f"{search_song} is found at index {target}")
                index.append(target)
        i=0
        print("this is the history for the song you search"+str(search_song))
        for a in index:
            print(list,i)
            print("-----------")
            print(artistlist[index[i]])
            print(titlelist[index[i]])
            print(genrelist[index[i]])
            print(yearlist[index[i]])
            print(commentlist[index[i]])
            artist_target = artistlist[index[i]]
            title_target = titlelist[index[i]]
            genre_target = genrelist[index[i]]
            year_target = yearlist[index[i]]
            comment_target = commentlist[index[i]]
            i=i+1

        return artist_target , title_target , genre_target , year_target , comment_target

        
    #this is a methot to clear the hystory for dubles entrys
    def clearThehystory(self):
        with open('output.csv','r') as f, open('clean.csv','w') as out_file:
            out_file.writelines(unique_everseen(f))
        shutil.copyfile("clean.csv", "output.csv")
    