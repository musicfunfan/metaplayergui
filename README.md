# What is this ?

This is program i made named MetaPlayerGui. It was a project for my university.

# What can you do with this ?

Is a player based on mpv and yt-dlp. You can use it search and watch youtube.com, local files, and all the sites that yt-dlp 
supports. You can also save the metadata from the media you use to a csv file if you want to.

# Why this is here ?

* Backup 
* share with friends
* share with world 

# Is this in active development ?

No i do not write code for that anymore. I may from time to time. But i do not really care for this project.

# How can i install it ?

* You need to run some kind of Linux distribution. I use archlinux.
* You need to have this is installed in your system.
* * gtk 3
* * yt-dlp (latest version)
* * python-mpv
* * tinytag
* * Python (Version 3)

If you install all that you need to clone this repo and run ``python MetaPlayer.py``

![IMAGE_DESCRIPTION](./screenshotMetaPlayerGui.png)


# Where is the saved metadata ?

In the programs file there is a file clean.csv there it is.
